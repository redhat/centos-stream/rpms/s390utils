#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/s390utils/Sanity/smoke-functionality
#   Description: Smoke, Sanity and function tests
#   Author: Dan Horák <sharkcz@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2019 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="s390utils-base"

EXTRACT="$PWD/extract-module-sig.pl"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest "General tests"
        rlRun "chccwdev --version"
        rlRun "chchp --version"
        rlRun "chcpumf --version"
        rlRun "chreipl --version"
        rlRun "chshut --version"
        rlRun "chzcrypt --version"
        rlRun "chzdev --version"
        rlRun "cio_ignore --version"
        rlRun "dasdfmt --version"
        rlRun "dasdinfo --version"
        rlRun "dasdstat --version"
        rlRun "dasdview --version"
        rlRun "fdasd --version"
        rlRun "hyptop --version"
        rlRun "lschp --version"
        rlRun "lscpumf --version"
        rlRun "lscss --version"
        rlRun "lsdasd --version"
        rlRun "lsqeth --version"
        rlRun "lsluns --version"
        rlRun "lsreipl --version"
        rlRun "lsscm --version"
        rlRun "lsshut --version"
        rlRun "lstape --version"
        rlRun "lszcrypt --version"
        rlRun "lszdev --version"
        rlRun "lszfcp --version"
        rlRun "qetharp --version"
        rlRun "qethconf --version"
        rlRun "qethqoat --version"
        rlRun "tape390_crypt --version"
        rlRun "tape390_display --version"
        rlRun "ttyrun --version"
        rlRun "tunedasd --version"
        rlRun "vmcp --version"
        rlRun "vmur --version"
        rlRun "zcryptctl --version"
        rlRun "zcryptstats --version"
        rlRun "zfcpdbf --version"
        rlRun "zgetdump --version"
        rlRun "zipl --version"
        rlRun "znetconf --version"
        rlRun "zpcictl --version"
        rlRun "zkey --version"
        rlRun "zkey-cryptsetup --version"
    rlPhaseEnd

    rlPhaseStartTest "s390utils 2.15.1+ - RHEL 8.4+ and RHEL 9"
        rlRun "hsci --version"
        rlRun "lsstp --version"

        # check for perl-free s390utils-core
        rlRun "rpm -q --requires s390utils-core | grep -q perl" 1 "Checking Perl deps in s390utils-core"

        # check kernel installation, must not create /boot/<machineid>
        if [ $UID -eq 0 ]; then
            if rlCheckRpm kernel-core; then
                rlRun "dnf -y reinstall kernel-core" 0 "Reinstalling the kernel to verify BLS scripts function"
                rlAssertNotExists /boot/`cat /etc/machine-id`
            else
                rlLog "Skipping kernel reinstallation / BLS test, kernel-core not installed, probably running in a container"
            fi
        else
            rlLog "Skipping kernel reinstallation / BLS test, needs root permissions"
        fi
    rlPhaseEnd

    rlPhaseStartTest "s390utils 2.16.0+ - RHEL 8.5+ and RHEL 9"
        rlRun "lshwc --version"
    rlPhaseEnd

    rlPhaseStartTest "s390utils 2.19.0+ - RHEL 8.6+ and RHEL 9"
        rlRun "hsavmcore --version"
        rlRun "sclpdbf --version"
        rlRun "zipl-editenv --version"
    rlPhaseEnd

    rlPhaseStartTest "s390utils 2.22.0+ - RHEL 8.7+ and RHEL 9.1+"
        rlRun "pai --version"
        rlRun "pvattest --version"
        rlRun "pvextract-hdr -h"
    rlPhaseEnd

    rlPhaseStartTest "s390utils 2.25.0+ - RHEL 8.8+ and RHEL 9.2+"
        # no new tools
    rlPhaseEnd

    rlPhaseStartTest "s390utils 2.27.0+ - RHEL 8.9+ and RHEL 9.3+"
        # no new tools
    rlPhaseEnd

    rlPhaseStartTest "s390utils 2.29.0+ - RHEL 8.10+ and RHEL 9.4+"
        rlRun "pvsecret --version"
    rlPhaseEnd

    rlPhaseStartTest "s390utils 2.33.1+ - RHEL 9.5+ and RHEL 10"
        rlRun "chpstat --version"
        rlRun "lspai --version"
    rlPhaseEnd

    rlPhaseStartTest "s390utils 2.36.0+ - RHEL 9.6+ and RHEL 10"
        rlRun "opticsmon --version"
        rlRun "cpacfinfo --version"
        rlRun "pvimg --version"
    rlPhaseEnd

    rlPhaseStartTest "Bootloader signature"
        # only RHEL has signed bootloader
        if rlIsRHEL '>=8'; then
            rlRun "$EXTRACT -s /usr/lib/s390-tools/stage3.bin > stage3.bin.sig" 0 "Check bootloader signature"
        else
            rlLog "Skipping bootloader signature check, not on RHEL"
        fi
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
